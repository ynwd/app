package main

import "testing"

func Test_add(t *testing.T) {
	tests := []struct {
		name string
		want int
	}{
		{
			name: "success",
			want: 1,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := add(); got != tt.want {
				t.Errorf("add() = %v, want %v", got, tt.want)
			}
		})
	}
}
